"""Arquivo que roda a aplicação."""
from main import app
import uvicorn

app = application = uvicorn.run(app, host="0.0.0.0", port=8000)
