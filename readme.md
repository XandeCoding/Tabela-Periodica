# Periodic Table - React e FastAPI :seedling:

Esta é uma ferramenta de consulta de elementos químicos que fiz para melhorar minhas habilidades com React e GraphQL usando Python no backend.

### Backend

O backend foi feito em python com o framework [FastAPI](https://fastapi.tiangolo.com/) que trata todas as requisições do frontend, e também mantém o GraphQL no ar o qual usei a biblioteca [Graphene](https://graphene-python.org/) para criar.

### Frontend

No foi utilizada a bibilioteca do [React](https://pt-br.reactjs.org/) e gostei bastante do esquema de components e de states que facilitam e que me deram bastante liberdade ao fazer o app, foi a primeira vez que utilizei então sou suspeito para falar.

![](/images-repo/graphql.png)
![](/images-repo/app.png)
